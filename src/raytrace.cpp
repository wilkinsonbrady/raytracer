#include "primitives.h"
#include "bmp.h"

#include <cmath>
#include <iostream>
#include <vector>


void raycast(BMP& img, double FOV, std::vector <sphere> obj_list, point_light light) {
    uint8_t A = 255;
    uint32_t channels = img.info_header.bit_count / 8;
    double FOV_rad = (FOV * (3.14/180))/2.0;

    double z = ((double)img.info_header.width/2.0) / tan(FOV_rad);
    std::cout << "FOV_rad: " << FOV_rad << std::endl;
    std::cout << "z: " << z << std::endl;

    uint32_t scaleF = 1;
    for (uint32_t y = 0; y < img.info_header.height; ++y) {
        for (uint32_t x = 0; x < img.info_header.width; ++x){
            vec currv((x-(img.info_header.width/2.0)),(y-(img.info_header.height/2.0)),z);

            double z_dist = 0;
            double intersect;
            vec close;
            double z_closest = 10000; //TODO NOT MAKE THIS RAND VAL
            sphere* closests=NULL;  
            for (int i = 0; i < obj_list.size(); ++i)
            {
                intersect = obj_list[i].intersection(currv);
                z_dist = intersect;
                if (z_dist > 0 && z_dist < z_closest){
                    z_closest = z_dist;
                    close = vec((x-(img.info_header.width/2.0)),(y-(img.info_header.height/2.0)),intersect);
                    closests = &obj_list[i];
                }
            }

            if (closests != NULL)
            {
                // std::cout << "-------------" << std::endl;
                // std::cout << "currv " << currv.x << '|' << currv.y << '|' << currv.z << std::endl;
                // std::cout << "close " << close.x << '|' << close.y << '|' << close.z << std::endl;
                // std::cout << "closests->center " << closests->center.x << '|' << closests->center.y << '|' << closests->center.z << std::endl;
                vec normv=close-closests->center;
                // std::cout << "normv " << normv.x << '|' << normv.y << '|' << normv.z << "||" << normv.mag() << std::endl;
                vec lightv=normv-light.location;
                // std::cout << "lightv " << lightv.x << '|' << lightv.y << '|' << lightv.z << std::endl;
                double ang = (light.location.angle(normv)); //Ummmmm I guess no work


                // std::cout << ang * 180/3.14 << std::endl;
                // std::cout << ang << std::endl;
                // RGB Col=closests->color*(lightv.angle(normv));
                // RGB Col=closests->color*ang;
                RGB Col=closests->color;
                // RGB Col=RGB(1,1,1)*ang* 180/3.14; //make color the angle


                img.data[channels * (y * img.info_header.width + x) + 2] = (Col.R);
                img.data[channels * (y * img.info_header.width + x) + 1] = (Col.G);
                img.data[channels * (y * img.info_header.width + x) + 0] = (Col.B);
                if (channels == 4) {
                    img.data[channels * (y * img.info_header.width + x) + 3] = A;
                }
            }
        }
    }

}




int main(int argc, char * argv []){
 
    std::cout << "*********bmp3*********" << std::endl;
     BMP bmp3(1920/5, 1080/5, true);
     double FOV = 90;


    std::vector <sphere> sp_obj_list;
    double r = 8;
    vec centerpt(0,0,100);
    vec right(30,0,0);
    vec up(0,30,0);
    vec back(0,0,30);

    RGB red(255,0,0);
    RGB green(0,255,0);
    RGB blue(0,0,255);
    RGB yellow(255,255,0);
    RGB cyan(0,255,255);
    RGB magenta(255,0,255);
    RGB white(255,255,255);



    //TODO DITHERING
    sp_obj_list.push_back(sphere(centerpt-up-right-back, r, cyan));
    sp_obj_list.push_back(sphere(centerpt+right-back, r, magenta));
    sp_obj_list.push_back(sphere(centerpt-right-back, r, red));
    sp_obj_list.push_back(sphere(centerpt+up-back, r, cyan));
    sp_obj_list.push_back(sphere(centerpt-up-back, r, blue));
    

    sp_obj_list.push_back(sphere(centerpt+right-up+back, r, yellow));
    sp_obj_list.push_back(sphere(centerpt+right+up+back, r, cyan));
    sp_obj_list.push_back(sphere(centerpt+right+back, r, red));
    sp_obj_list.push_back(sphere(centerpt-right+back, r, magenta));
    sp_obj_list.push_back(sphere(centerpt+up+back, r, yellow));
    sp_obj_list.push_back(sphere(centerpt-up+back, r, green));

    sp_obj_list.push_back(sphere(centerpt-up, r, red));
    sp_obj_list.push_back(sphere(centerpt+up, r, green));
    sp_obj_list.push_back(sphere(centerpt-right, r, yellow));
    sp_obj_list.push_back(sphere(centerpt+right, r, blue));

    
    point_light light(centerpt, 1, white);
    sp_obj_list.push_back(sphere(centerpt, 2, white));

    raycast(bmp3, FOV, sp_obj_list, light);


    double pt1 = sphere(centerpt-right+back, r, magenta).intersection(vec(-30,0,110));
    double pt2 = sphere(centerpt-right, r, yellow).intersection(vec(-30,0,110));
    point_light l1(centerpt+up, 1);
    std::cout << "farTEST: " << pt1 << '|' << l1.location.x << '|' << l1.location.y << '|' << l1.location.z << std::endl;
    std::cout << "closeTEST: " << pt2 << std::endl;

    vec x = vec(0,0,100)-vec(0,0,90);
    vec y = vec(0,10,90);
    std::cout << "x: " << x.x << '|' << x.y << '|' << x.z << std::endl;
    std::cout << "y: " << y.x << '|' << y.y << '|' << y.z << std::endl;
    bmp3.write("img_test.bmp");



    return 0;
}