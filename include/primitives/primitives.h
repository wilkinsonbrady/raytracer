#ifndef __PRIMITIVES_H
#define __PRIMITIVES_H

#include <cmath>
#include <iostream>
#include <stdint.h>
#include <cstddef>

struct RGB
{
    uint8_t R, G, B = 0;
    RGB(uint8_t R0=255, uint8_t G0=255, uint8_t B0=0);

    RGB operator*(double x);
    void operator^(const double mult);
    RGB operator/(double x);
};

class vec
{
    public:
    double x, y, z = 0;
    vec(double x0, double y0, double z0);
    vec();

    vec operator-(const vec& v);
    vec operator+(const vec& v);
    vec operator*(const double& x);
    vec cross(vec v);
    double dot(vec v);
    double mag();
    double angle(vec v);
    vec normalize();
};

class point_light
{
    public:
    vec location;
    RGB color;
    double brightness;

    point_light(vec loc, double brightness0, RGB colorvals=RGB());
    RGB lightval(vec v, vec normvec);

};

class sphere
{
    public: 
        vec center;
        double r = 0;
        double r_sq = 0;
        RGB color;
        
        sphere(vec location, double r0, RGB colorvals=RGB(255,255,0));

        double intersection(vec v);
};




class line //CURSED CLASS
{
    public:
        vec p0, p1, pnorm;
        double width = 1;

        RGB color;

        line(double x0, double y0, double z0, double x1, double y1, double z1, double width0, uint8_t R = 255, uint8_t G = 255, uint8_t B = 0){
            width = width0;
            p0 = vec(x0,y0,z0);
            p1 = vec(x1,y1,z1);
            pnorm = p0.cross(p1);
            color.R = R;
            color.G = G;
            color.B = B;
        }

        double intersection(vec v){
            double z = pnorm.dot(v);
            // if (x < width) {
            //     z_depth = h/sin(x);
            // }
            if (z==1){
                return z;
            }
            else 
                return NULL;
        }
};

#endif