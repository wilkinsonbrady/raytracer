#include "primitives.h"
#include <iostream>

/////////////RGB//////////////
RGB::RGB(uint8_t R0, uint8_t G0, uint8_t B0){
    this->R = R0;
    this->G = G0;
    this->B = B0;
}
RGB RGB::operator*(double x){
    RGB OUT;
    OUT.R = this->R * x;
    OUT.G = this->G * x;
    OUT.B = this->B * x;
    return OUT;
}
void RGB::operator^(const double mult){ //Clipping mult
    double multx = (double)this->R*mult;
    this->R = multx > 255 ? 255 : multx;

    double multy = (double)this->G*mult;
    this->G = multy > 255 ? 255 : multy;

    double multz = (double)this->B*mult;
    this->B = multz > 255 ? 255 : multz;
}
RGB RGB::operator/(double x){
    RGB OUT;
    OUT.R = this->R / x;
    OUT.G = this->G / x;
    OUT.B = this->B / x;
    return OUT;
}


/////////////VEC//////////////
vec::vec(double x0, double y0, double z0){x = x0; y = y0; z = z0;}
vec::vec(){x = 0; y = 0; z = 0;}
vec vec::operator-(const vec& v){
    vec out;
    out.x = this->x-v.x;
    out.y = this->y-v.y;
    out.z = this->z-v.z;
    return out;
}
vec vec::operator+(const vec& v){
    vec out;
    out.x = this->x+v.x;
    out.y = this->y+v.y;
    out.z = this->z+v.z;
    return out;
}
vec vec::operator*(const double& x){
    vec out;
    out.x = this->x*x;
    out.y = this->y*x;
    out.z = this->z*x;
    return out;
}
vec vec::cross(vec v){
    vec out;
    out.x = this->y * v.z - this->z * v.y;
    out.y = this->z * v.x - this->x * v.z;
    out.z = this->x * v.y - this->y * v.x;
    return out;
}
double vec::dot(vec v){
    double out = this->x * v.x + this->y * v.y + this->z * v.z;
    return out; 
}
double vec::mag(){
    double out = sqrt(pow(this->x,2) + pow(this->y,2) + pow(this->z,2));
    return out;
}
double vec::angle(vec v){
    return acos(this->dot(v)/(this->mag() * v.mag())); //FIXME Sometimes returns NAN when vectors line up
}
vec vec::normalize(){
    double mag = this->mag();
    return vec(this->x/mag, this->y/mag, this->z/mag);
}

////////POINT LIGHT///////
point_light::point_light(vec loc, double brightness0, RGB colorvals){
    location = loc;
    brightness = brightness0;
    color = colorvals;
}
RGB point_light::lightval(vec v, vec normvec){ //normvec
    vec test = location - v;
    double ang = test.angle(normvec);
    RGB col;
    col.R =  0;
    col.B =  0;
    col.G =  0;
    if (ang > (3.1415/2.0)){
        col.R =  255;
        col.B =  255;
        col.G =  255;
        double normAng = ang / (3.1415);
        std::cout << normAng << '|' << ang * 180/3.1415 << std::endl;
        std::cout << "=======" << std::endl;
        col = col * normAng;
        return col;
    }
    else{
        return col;
    }
}


////////SPHERE///////
sphere::sphere(vec location, double radius, RGB colorvals){
    r=radius;
    r_sq=pow(radius,2);
    center = location;
    color = colorvals;
}
double sphere::intersection(vec v){
    double z_depth0 = -1;
    double magCenter = this->center.mag();
    double x = this->center.angle(v); //angle between direction and cast ray
    double h = magCenter * sin(x); //norm dis between center and vector
    if (h >= 0 && h <= r && x <= 3.14/2 && x >= -3.14/2) {
        double h_sq = pow(h,2);
        double y = sqrt(h_sq + r_sq);
        double len = sqrt(h_sq + pow(magCenter,2));
        z_depth0 = len-y;
        // std::cout << z_depth0 << " " << len << " " << y << " " << x << std::endl;
        // z_depth1 = len+y;
    }
    // return z_depth1 > z_depth0 ? z_depth0 : z_depth1;
    return z_depth0;
}